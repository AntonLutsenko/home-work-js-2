
// Циклы необходимы программисту для многократного выполнения одного и того же кода, пока истинно какое-то условие. Если условие всегда истинно, то такой цикл называется бесконечным, у такого цикла нет точки выхода.

;
let userNumber;
let resultConsole = 0;
const message = 'Sorry, no number';

do {
    userNumber = +prompt('Enter you number!');
} while (Number.isInteger(userNumber) === false);
    
for (let i = 0; i <= userNumber; i++ ) {
   if (i % 5 === 0 && i > 0) {
    console.log(i);
    resultConsole = i;
   }  
} if (resultConsole === 0) {
    console.log(message);
   } 


// 2й вариант 

// let userNumber;
// let resultConsole;
// const message = 'Sorry, no number';

// do {
//     userNumber = +prompt('Enter you number!');
// } while (Number.isInteger(userNumber) === false);

// if (userNumber === 5) {
//     console.log(userNumber)
// } else if (userNumber < 5) {
//     console.log(message)
// } else {
//     for (let i = 0; i <= userNumber; i++) {
//         if (i % 5 === 0 && i > 0) {
//             console.log(i);
//             resultConsole = i;
//         }
//     }
// }